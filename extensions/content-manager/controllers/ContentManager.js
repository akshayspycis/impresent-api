'use strict';
const _ = require('lodash');
const { map, filter, each } = require('lodash/fp');
const { AbilityBuilder, Ability } = require('@casl/ability');
const sift = require('sift');

const { parseMultipartData, sanitizeEntity } = require('strapi-utils');

const _user = { "isActive": true, "blocked": false, "roles": [{ "_id": "5f29b43d9d61773a08aafdd2", "name": "Super Admin", "code": "strapi-super-admin", "description": "Super Admins can access and manage all features and settings.", "createdAt": "2020-08-04T19:17:17.417Z", "updatedAt": "2020-08-04T19:17:17.417Z", "__v": 0, "id": "5f29b43d9d61773a08aafdd2" }], "_id": "5f28536202f70b0748977058", "username": "stayout", "password": "$2a$10$q5i/scobHcjM11zra5WoDeHmRmwzq4lb1oWhP.j9krcnDu.BDTWte", "email": "stayout@gmail.com", "createdAt": "2020-08-03T18:11:46.381Z", "updatedAt": "2020-10-04T19:50:27.168Z", "__v": 0, "firstname": "Akshay", "lastname": "Bilani", "id": "5f28536202f70b0748977058" };
const allowedOperations = [
  '$or',
  '$eq',
  '$ne',
  '$in',
  '$nin',
  '$lt',
  '$lte',
  '$gt',
  '$gte',
  '$exists',
  '$elemMatch',
];
const operations = _.pick(sift, allowedOperations);

const conditionsMatcher = conditions => {
  return sift.createQueryTester(conditions, { operations });
};

const ACTIONS = {
  read: 'plugins::content-manager.explorer.read',
  create: 'plugins::content-manager.explorer.create',
  edit: 'plugins::content-manager.explorer.update',
  delete: 'plugins::content-manager.explorer.delete',
};

const findEntityAndCheckPermissions = async (ability, action, model, id) => {
  const contentManagerService = strapi.plugins['content-manager'].services.contentmanager;
  const entity = await contentManagerService.fetch(model, id);

  if (_.isNil(entity)) {
    throw strapi.errors.notFound();
  }

  const roles = _.has(entity, 'created_by.id')
    ? await strapi.query('role', 'admin').find({ users: entity.created_by.id }, [])
    : [];
  const entityWithRoles = _.set(_.cloneDeep(entity), 'created_by.roles', roles);

  const pm = strapi.admin.services.permission.createPermissionsManager(ability, action, model);

  // if (pm.ability.cannot(pm.action, pm.toSubject(entityWithRoles))) {
  //   throw strapi.errors.forbidden();
  // }

  return { pm, entity };
};

module.exports = {
  async evaluatePermission({ permission, user, options, registerFn }) {
    const { action, fields, conditions } = permission;
    const subject = permission.subject || 'all';

    // Permissions with empty fields array should be removed
    if (Array.isArray(fields) && fields.length === 0) {
      return;
    }

    // Directly registers the permission if there is no condition to check/evaluate
    if (_.isUndefined(conditions) || _.isEmpty(conditions)) {
      return registerFn({ action, subject, fields, condition: true });
    }

    // Replace each condition name by its associated value
    const resolveConditions = map(conditionProvider.getById);

    // Only keep the handler of each condition
    const pickHandlers = map(_.property('handler'));

    // Filter conditions, only keeps objects and functions
    const filterValidConditions = filter(_.isObject);

    // Evaluate the conditions if they're a function, returns the object otherwise
    const evaluateConditions = conditions =>
      Promise.all(conditions.map(cond => (_.isFunction(cond) ? cond(user, options) : cond)));

    // Only keeps 'true' booleans or objects as condition's result
    const filterValidResults = filter(result => result === true || _.isObject(result));

    // Transform each result into registerFn options
    const transformToRegisterOptions = map(result => ({
      action,
      subject,
      fields,
      condition: result,
    }));

    // Register each result using the registerFn
    const registerResults = each(registerFn);

    await Promise.resolve(conditions)
      .then(resolveConditions)
      .then(pickHandlers)
      .then(filterValidConditions)
      .then(evaluateConditions)
      .then(filterValidResults)
      .then(transformToRegisterOptions)
      .then(registerResults);
  },

  createRegisterFunction(can) {
    return ({ action, subject, fields, condition }) => {
      return can(action, subject, fields, _.isObject(condition) ? condition : undefined);
    };
  },

  checkMany: _.curry((ability, permissions) => {
    return permissions.map(({ action, subject, field }) => ability.can(action, subject, field));
  }),

  async _generateUserAbility(user, options) {
    const permissions = await strapi.admin.services.permission.findUserPermissions(user);
    const abilityCreator = this.generateAbilityCreatorFor(user);

    return abilityCreator(permissions, options);
  },

  generateAbilityCreatorFor(user) {
    return async (permissions, options) => {
      const { can, build } = new AbilityBuilder(Ability);
      const registerFn = this.createRegisterFunction(can);

      for (const permission of permissions) {
        await this.evaluatePermission({ permission, user, options, registerFn });
      }

      return build({ conditionsMatcher });
    };
  },


  async createRecord(ctx) {
    const {
      state: { user },
      params: { model },
      request: { body },
    } = ctx;
    const contentManagerService = strapi.plugins['content-manager'].services.contentmanager;



    let userAbility = await this._generateUserAbility(_user);

    // const userPermissions = await findUserPermissions(_user);

    // console.log("auserPermissions.map(sanitizePermission)", userPermissions.map(sanitizePermission))

    const pm = strapi.admin.services.permission.createPermissionsManager(
      userAbility,
      ACTIONS.create,
      model
    );

    // if (!pm.isAllowed) {
    //   throw strapi.errors.forbidden();
    // }

    const sanitize = e => pm.pickPermittedFieldsOf(e, { subject: model });

    const { data, files } = ctx.is('multipart') ? parseMultipartBody(ctx) : { data: body };

    try {
      const result = await contentManagerService.create(
        {
          data: { ...sanitize(data), created_by: user.id, updated_by: user.id, createdby: user.id },
          files,
        },
        { model }
      );

      ctx.body = pm.sanitize(result, { action: ACTIONS.read });

      await strapi.telemetry.send('didCreateFirstContentTypeEntry', { model });
    } catch (error) {
      strapi.log.error(error);
      ctx.badRequest(null, [
        {
          messages: [{ id: error.message, message: error.message, field: error.field }],
          errors: _.get(error, 'data.errors'),
        },
      ]);
    }
  },

  async findRecord(ctx) {
    const { model } = ctx.params;
    const { query } = ctx.request;

    const contentManagerService = strapi.plugins['content-manager'].services.contentmanager;

    const { kind } = strapi.getModel(model);
    let userAbility = await this._generateUserAbility(_user);

    const pm = strapi.admin.services.permission.createPermissionsManager(
      userAbility,
      ACTIONS.read,
      model
    );

    if (kind === 'singleType') {
      // fetchAll for a singleType only return one entity
      const entity = await contentManagerService.fetchAll(model, query);

      // allow user with create permission to know a single type is not created
      if (!entity) {
        if (pm.ability.cannot(ACTIONS.create, model)) {
          // return ctx.forbidden();
        }

        return ctx.notFound();
      }

      if (pm.ability.cannot(ACTIONS.read, pm.toSubject(entity))) {
        // return ctx.forbidden();
      }

      return (ctx.body = pm.sanitize(entity));
    }

    if (pm.ability.cannot(ACTIONS.read, model)) {
      // return ctx.forbidden();
    }

    const method = _.has(query, '_q') ? 'search' : 'fetchAll';
    const queryParameters = pm.queryFrom(query);
    console.log(JSON.stringify( queryParameters))
    // const results = await db.query(model).search(params);
    const results = await contentManagerService[method](model, queryParameters);
    if (!results) {
      return ctx.notFound();
    }
    ctx.body = pm.sanitize(results);
    console.log(JSON.stringify(ctx.body))
  },

  async findOneRecord(ctx) {
    const {
      params: { model, id },
    } = ctx;

    let userAbility = await this._generateUserAbility(_user);

    const { pm, entity } = await findEntityAndCheckPermissions(
      userAbility,
      ACTIONS.read,
      model,
      id
    );

    ctx.body = pm.sanitize(entity);
  },

  async updateRecord(ctx) {
    const {
      state: {  user },
      params: { id, model },
      request: { body },
    } = ctx;
    try {
    console.log("user");
    let userAbility = await this._generateUserAbility(_user);
    console.log("user 0");
    const contentManagerService = strapi.plugins['content-manager'].services.contentmanager;
    console.log("user 00");
    const { pm, entity } = await findEntityAndCheckPermissions(
      userAbility,
      ACTIONS.edit,
      model,
      id
    );

    console.log("user 1");
    const sanitize = e => pm.pickPermittedFieldsOf(e, { subject: pm.toSubject(entity) });
    console.log("user 11");
    const { data, files } = ctx.is('multipart') ? parseMultipartBody(ctx) : { data: body };
    console.log("user 112");

      const result = await contentManagerService.edit(
        { id },
        { data: { ...sanitize(_.omit(data, ['created_by'])), updated_by: user.id }, files },
        { model }
      );

      ctx.body = pm.sanitize(result, { action: ACTIONS.read });
      console.log("user 1");
    } catch (error) {
      console.log(error)
      strapi.log.error(error);
      ctx.badRequest(null, [
        {
          messages: [{ id: error.message, message: error.message, field: error.field }],
          errors: _.get(error, 'data.errors'),
        },
      ]);
    }
  },
  
  async deleteRecord(ctx) {
    const {
      params: { id, model },
    } = ctx;

    let userAbility = await this._generateUserAbility(_user);
    const contentManagerService = strapi.plugins['content-manager'].services.contentmanager;

    const { pm } = await findEntityAndCheckPermissions(userAbility, ACTIONS.delete, model, id);

    const result = await contentManagerService.delete(model, { id });

    ctx.body = pm.sanitize(result, { action: ACTIONS.read });
  },
  
  async countRecord(ctx) {
    const {
      params: { model },
      request,
    } = ctx;

    let userAbility = await this._generateUserAbility(_user);

    const contentManagerService = strapi.plugins['content-manager'].services.contentmanager;
    

    const pm = strapi.admin.services.permission.createPermissionsManager(
      userAbility,
      ACTIONS.read,
      model
    );
    const method = _.has(request.query, '_q') ? 'countSearch' : 'count';
    const query = pm.queryFrom(request.query);

    const count = await contentManagerService[method](model, query);

    ctx.body = {
      count: _.isNumber(count) ? count : _.toNumber(count),
    };
  },

};
